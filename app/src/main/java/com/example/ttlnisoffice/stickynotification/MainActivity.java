package com.example.ttlnisoffice.stickynotification;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.TaskStackBuilder;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        
        buildNotification();
    }

    Notification notification;
    //Notifications in Android are represented by the Notification class
    //They are built using with Notification.Builder
    //Ther are displayed using NotificationManager

    private void buildNotification() {

        Intent intent = new Intent(this, MainActivity.class); //required for PendingIntent

        TaskStackBuilder stackBuilder = TaskStackBuilder.create(this);

        stackBuilder.addParentStack(MainActivity.class);

        stackBuilder.addNextIntent(intent);

        //A PendingIntent object helps you to perform an action on your applications behalf, often
        // at a later time, without caring of whether or not your application is running.

        //A PendingIntent is a intent that you give to a foreign application (e.g. NotificationManager,
        //AlarmManager, Home Screen AppWidgetManager...) which allows the foreign application to use
        //your application's permissions to execute a predefined piece of code
        PendingIntent pendingIntent = stackBuilder.getPendingIntent(
                0,
                PendingIntent.FLAG_UPDATE_CURRENT); //if this intent exists update it, don't create new one

        //Notification.Builder allows you to add up to three buttons
        Notification.Builder builder = new Notification.Builder(this)
                .setContentTitle("No tasks today")
                .setContentText("Have a nice day!")
                .setSmallIcon(android.R.drawable.arrow_down_float)
                .setContentIntent(pendingIntent)
                .addAction(R.drawable.ic_notif_add, "Add", pendingIntent)
                .addAction(R.drawable.ic_notif_settings, "Settings", pendingIntent);




        if (Build.VERSION.SDK_INT <= Build.VERSION_CODES.JELLY_BEAN){
            notification = builder.build();
        } else {
            notification = builder.getNotification();
        }

        notification.flags = Notification.FLAG_NO_CLEAR | Notification.FLAG_ONGOING_EVENT;



        NotificationManager manager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);

        manager.notify(123, notification);
    }
}
